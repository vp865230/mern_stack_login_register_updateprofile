import { Flex } from '@chakra-ui/react';
import { BrowserRouter, Route , Routes} from 'react-router-dom';

import Header from "./components/Header";
import HomeScreen from './screens/HomeScreen';
import LoginScreen from './screens/LoginScreen';
import RegisterScreen from './screens/RegisterScreen';
import ProfileScreen from './screens/ProfileScreen';


const App = () => {
  return (
    <BrowserRouter>
        < Header />
        <Flex
          as='main'
          mt='72px'
          direction='column'
          minH='xl'
          py='6'
          px='6'
          bgColor='blackAlpha.700'>
            <Routes>
              < Route path='/' element={<HomeScreen />} />
              < Route path='/login' element={<LoginScreen />} />
              < Route path='/register' element={<RegisterScreen />} />
              < Route path= '/profile' element={<ProfileScreen />} />
            </Routes> 
        </Flex>
    </BrowserRouter>
  )
}

export default App;