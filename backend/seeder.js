import colors from 'colors';
import dotenv from 'dotenv';
import mongoose from 'mongoose';

import connectDb from './config/db.js';
import users from './data/users.js';

import User from './models/userModel.js';



dotenv.config();

connectDb();

const importData = async () => {
    try {
        await User.deleteMany();

        await User.insertMany(users);

        console.log('Data importd'.green.inverse);
        process.exit();
    }catch (err) {
        console.error(`${err}`.red.inverse);
        process.exit(1);
    }
};

const destroyData = async () => {
    try {
        await User.deleteMany();
        console.log(`Data destroyed`.red.inverse);
    }catch (err) {
        console.error(`${err}`.red.inverse)
        process.exit(1);
    }
};

if (process.argv[2] === '-d') {
    destroyData();
}else {
    importData();
}