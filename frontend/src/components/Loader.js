import { Flex, Spinner } from "@chakra-ui/react";

const Loader = () => {
    return (
        <Flex alignItems='center' justifyContent='center'>
            <Spinner
                thickness="4px"
                speed="0.35s"
                emptyColor="black"
                color="red"
                size='xl'
                labal='Loading...'
            />
        </Flex>
    )
}

export default Loader;