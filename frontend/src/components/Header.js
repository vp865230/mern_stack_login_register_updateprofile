import {
	Box,
	Button,
	Flex,
	Heading,
	Icon,
	Link,
	Menu,
	MenuButton,
	MenuItem,
	MenuList,
} from '@chakra-ui/react';
import { useState } from 'react';
import { HiOutlineMenuAlt3, HiUser } from 'react-icons/hi';
import { IoChevronDown } from 'react-icons/io5';
import { useDispatch, useSelector } from 'react-redux';
import { Link as RouterLink, useNavigate } from 'react-router-dom';
import { logout } from '../action/userActions';

const Header = () => {
	const dispatch = useDispatch();
	const navigate = useNavigate();

	const [show, setShow] = useState(false);

	const userLogin = useSelector((state) => state.userLogin);
	const { userInfo } = userLogin;

	const logoutHandler = () => {
		dispatch(logout());
		navigate('/');
	};

	return (
		<Flex
			as='header'
			align='center'
			justifyContent='space-between'
			wrap='wrap'
			py='6'
			px='6'
			bgColor='gray.800'
			w='100%'
			top='0'
			pos='fixed'
			zIndex='10'>
			{/* Title */}
			<Heading
				as='h1'
				color='whiteAlpha.800'
				fontWeight='bold'
				size='md'
				letterSpacing='md'>
				<Link
					as={RouterLink}
					to='/'
					_hover={{ color: 'orange', textDecor: 'none' }}>
					Test User Login
				</Link>
			</Heading>

			{/* Hamburger Menu */}
			<Box
				display={{ base: 'block', md: 'none', sm: 'block' }}
				onClick={() => setShow(!show)}>
				<Icon as={HiOutlineMenuAlt3} color='white' w='6' h='6' />
			</Box>

			{/* Menu */}
			<Box
				display={{ base: show ? 'block' : 'none', md: 'flex' }}
				width={{ base: 'full', md: 'auto' }}
				alignItems='center'>
				{userInfo ? (
					<Menu>
						<MenuButton
							as={Button}
							rightIcon={<IoChevronDown />}
							_hover={{ textDecor: 'none', opacity: '0.7' }}>
							{userInfo.name}
						</MenuButton>
						<MenuList>
							<MenuItem as={RouterLink} to='/profile'>
								Profile
							</MenuItem>
							<MenuItem onClick={logoutHandler}>Logout</MenuItem>
						</MenuList>
					</Menu>
				) : (
					<Link
						as={RouterLink}
						to='/login'
						fontSize='sm'
						letterSpacing='wide'
						color='whiteAlpha.600'
						fontWeight='bold'
						textTransform='uppercase'
						mr='5'
						display='flex'
						alignItems='center'
						_hover={{ color: 'orange' }}
						mt={{ base: 4, md: 0 }}>
						<Icon as={HiUser} mr='1' w='4' h='4' />
						Login
					</Link>
				)}
			</Box>
		</Flex>
	);
};

export default Header;

